package sopra.battlecode.wackamole.footballgame;

/**
 * It's all about a football match where the cages get rattled!
 */
public class App
{
  /**
   * Main method.
   *
   * @param args
   */
  public static void main(String[] args)
  {
    App app = new App();
    
    app.runFor(90, 35, 65, 35);
    app.runFor(90, 15, 30, 10);
    app.runFor(90, 1, 120, 2);
  }
  
  /**
   * Wrapper for the run method, prints nice words.
   *
   * @param numPacks
   */
  private void runFor(int matchLength, int homeScoreInterval, int homeConcedeInterval, int awayScoreInterval)
  {
    System.out.println("Playing football match for.............: [" + matchLength + "] mins.");
    System.out.println("Home team scores a goal every..........: [" + homeScoreInterval + "] mins.");
    System.out.println("Home team concedes an own-goal every...: [" + homeConcedeInterval + "] mins.");
    System.out.println("Away team scores a goal every..........: [" + awayScoreInterval + "] mins.");
    
    run(matchLength, homeScoreInterval, homeConcedeInterval, awayScoreInterval);
  }
  
  /**
   * Runs the app.
   *
   * @param numPacks
   */
  private void run(int matchLength, int homeScoreInterval, int homeConcedeInterval, int awayScoreInterval)
  {
    int homeTeamGoals    = Math.abs(matchLength / homeScoreInterval);
    int homeTeamOwnGoals = Math.abs(matchLength / homeConcedeInterval);
    int awayTeamGoals    = Math.abs(matchLength / awayScoreInterval);
    
    StringBuffer sb = new StringBuffer();
    
    sb.append("Final Score: ");
    sb.append(homeTeamGoals);
    sb.append("-");
    sb.append(homeTeamOwnGoals + awayTeamGoals);
    
    System.out.println(sb.toString());
    System.out.println("");
    
  }
}
