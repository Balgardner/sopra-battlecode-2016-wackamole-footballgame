# wackamole.footballgame

A solution for the 'Football Game' problem, #1.

# Running this application.

This is a Maven Java project. Simply enter the following at the command-line:

```
mvn exec:java
```

# Results of running application.

Based on the specs of the problem, the answers to the question are:

```
Playing football match for.............: [90] mins.
Home team scores a goal every..........: [35] mins.
Home team concedes an own-goal every...: [65] mins.
Away team scores a goal every..........: [35] mins.
Final Score: 2-3

Playing football match for.............: [90] mins.
Home team scores a goal every..........: [15] mins.
Home team concedes an own-goal every...: [30] mins.
Away team scores a goal every..........: [10] mins.
Final Score: 6-12

Playing football match for.............: [90] mins.
Home team scores a goal every..........: [1] mins.
Home team concedes an own-goal every...: [120] mins.
Away team scores a goal every..........: [2] mins.
Final Score: 90-45
```